import { Line, mixins } from 'vue-chartjs'

export default {
  extends: Line,
  mixins: [mixins.reactiveProp],
  props: ['chartData', 'label', 'border', 'labels'],
  data () {
    return {
      gradient: null
    }
  },
  mounted () {
    this.gradient = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450)

    this.gradient.addColorStop(0.5, 'rgba(75,192,192,0.4)')
    this.gradient.addColorStop(0.5, 'rgba(75,192,192,0.4)')
    this.gradient.addColorStop(1, 'rgba(255, 255, 255, 0)')

    this.renderChart({
      labels: this.labels,
      datasets: [
        {
          label: this.label,
          backgroundColor: this.gradient,
          borderColor: this.border,
          borderCapStyle: 'butt',
          pointBorderColor: 'rgba(94, 114, 228)',
          data: this.chartData
        }
      ]
    }, {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            padding: 20,
            fontColor: '#808080'
          },
          gridLines: {
            drawTicks: false,
            display: false
          }
        }],
        xAxes: [{
          ticks: {
            padding: 20,
            fontColor: '#808080'
          },
          gridLines: {
            drawTicks: false,
            color: '#808080',
            zeroLineColor: '#808080'

          }
        }]
      }
    })
  }
}
