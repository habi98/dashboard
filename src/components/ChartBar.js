import { Bar } from 'vue-chartjs'

export default {
  extends: Bar,
  data () {
    return {
      gradient: null
    }
  },
  mounted () {
    this.gradient = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450)

    this.gradient.addColorStop(0.5, 'rgba(75,192,192,0.4)')
    this.gradient.addColorStop(0.3, 'rgba(75,192,192,0.4)')
    this.gradient.addColorStop(1, 'rgba(255, 255, 255, 0)')

    this.renderChart({
      labels: ['Jan', 'Feb', 'May', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      datasets: [{
        label: 'GitHub Commits',

        backgroundColor: this.gradient,
        borderWidth: 2,
        barPercentage: 0.7,
        categoryPercentage: 0.8,
        data: [40, 30, 25, 39, 15, 40, 39, 60, 40, 20, 12, 11]
      }]
    }, {
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            padding: 20
          },
          gridLines: {
            drawTicks: false,
            display: true,
            zeroLineWidth: 0.5

          }
        }],
        xAxes: [{
          ticks: {
            padding: 20
          },
          gridLines: {
            display: false
          }
        }]
      },
      responsive: true,
      maintainAspectRatio: false
    })
  }
}
