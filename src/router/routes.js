
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', name: 'Dashboard', component: () => import('pages/Dashboard.vue') },
      { path: '/user', name: 'User Profile', component: () => import('pages/UserPage.vue') },
      { path: '/table', name: 'Table', component: () => import('pages/Table.vue') },
      { path: '/natifications', name: 'Natifications', component: () => import('pages/NatificationsPage.vue') },
      { path: '/calendar', name: 'Calendar', component: () => import('pages/CalendarPage.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
