import Vue from 'vue'
import Vuex from 'vuex'

import index from './modules/index'
import events from './modules/store-calendar'

Vue.use(Vuex)

export default function () {
  const Store = new Vuex.Store({
    modules: {
      index,
      events
    },
    strict: process.env.DEV
  })
  return Store
}
