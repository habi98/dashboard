import { uid } from 'quasar'

const state = {
  events: [
    {
      title: 'April Fools Day',
      details: 'Everything is funny as long as it is happening to someone else',
      date: '2020-04-01',
      bgcolor: '#ed9128',
      id: uid()
    },
    {
      title: 'Lunch',
      details: 'Company is paying!',
      date: '2020-04-15',
      time: '12:00',
      bgcolor: '#82cc0c',
      duration: 90,
      id: uid()
    },
    {
      title: 'Fishing',
      details: 'Time for some weekend R&R!',
      date: '2020-04-18',
      bgcolor: '#9f02d4',
      id: uid()
    },
    {
      title: 'Conference',
      details: 'Teaching Javascript 101',
      date: '2020-04-20',
      time: '08:00',
      duration: 590,
      bgcolor: 'blue',
      id: uid()
    }
  ]
}

const actions = {
  saveEvent ({ commit }, eventData) {
    commit('saveEvent', eventData)
  },
  deleteEvent ({ commit }, id) {
    commit('deleteEvent', id)
  },
  editEvent ({ commit }, data) {
    commit('editEvent', data)
  }
}

const mutations = {
  saveEvent (state, data) {
    state.events = [...state.events, data]
  },
  deleteEvent (state, id) {
    const newEvents = [...state.events]
    const index = newEvents.findIndex(event => event.id === id)

    newEvents.splice(index, 1)
    state.events = newEvents
  },
  editEvent (state, data) {
    const newEvents = [...state.events]
    const index = newEvents.findIndex(event => event.id === data.id)
    newEvents[index] = data.data
    state.events = newEvents
  }
}

const getters = {
  events (state) {
    return state.events
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
